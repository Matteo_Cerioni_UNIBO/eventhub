<?php

/* @var $this View */
/* @var $content string */

use common\models\LoginForm;
use common\widgets\Alert;
use frontend\assets\AppAsset;
use frontend\models\form\SignupForm;
use frontend\widgets\Growl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" type="image/jpg" href="<?= Yii::getAlias("@web") . "/img/favicon.jpg" ?>"/>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php $this->beginBody() ?>

<nav class="navbar navbar-expand-lg navbar-dark fixed-top ">
    <a class="navbar-brand" href="<?= Url::to(['site/index']) ?>">
        <img src="<?= Yii::getAlias('@web/img/logo.png') ?>" class="d-inline-block align-top" alt="logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <form class="search-content form-inline col-lg-9 justify-content-center" action="<?= Url::to(['event/search']) ?>">
            <!-- <input class="form-control searchicon" type="image" src="../img/searchicon.png"> -->
            <input class="form-control searchbar w-75" type="search" placeholder="" autocomplete="off"
                   autofocus="autofocus" name="q">
            <button class="btn btn-filter" type="submit">Cerca</button>
        </form>
        <ul class="navbar-nav ml-auto">
            <?php if (Yii::$app->user->isGuest): ?>
                <li class="nav-item">
                    <button type="button" class="btn btn-navbar" data-toggle="modal"
                            data-target="#modal-login">Log In
                    </button>
                </li>
                <li class="nav-item">
                    <button type="button" class="btn btn-navbar" data-toggle="modal"
                            data-target="#modal-register">Registrati
                    </button>
                </li>
            <?php else: ?>
                <!-- <li class="nav-item">
                  <a class="btn btn-outline-success"><span class="fa fa-user">Carrello</a>
                </li> -->
                <?php if (Yii::$app->user->identity->isManager()): ?>
                    <li class="nav-item">
                        <a href="<?= Url::to(['personal-area/dashboard']) ?>" class="btn btn-navbar"><span
                                class="fas fa-user-shield"> Area Personale</a>
                    </li>
                <?php else: ?>
                    <?php
                    $cartCount = Yii::$app->user->identity->getTickets()->carrello()->count();
                    ?>
                    <li class="nav-item">
                        <a href="<?= Url::to(['event/cart']) ?>"
                           class="btn btn-navbar <?= $cartCount == 0 ? 'disabled' : '' ?>">
                            <i class="fa fa-cart-plus"></i>
                            <span class="d-md-none"> Carrello</span>
                            <span class="badge badge-info"><?= $cartCount ?></span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="btn btn-navbar" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Account
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= Url::to(['personal-area/tickets']) ?>"><i
                                        class="fas fa-ticket-alt"></i> I miei biglietti</a>
                            <a class="dropdown-item" href="<?= Url::to(['personal-area/detail']) ?>"><i
                                        class="fas fa-cog"></i> Opzioni</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?= Url::to(['site/logout']) ?>"><i
                                        class="fas fa-sign-out-alt"></i> Logout</a>
                        </div>
                    </li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
    </div>
</nav>
<div class="wrap d-flex min-vh-100 flex-column">
    <?= Alert::widget() ?>
    <?= Growl::widget() ?>
    <?= $content ?>
</div>

<!-- Log In Modal -->
<div id="modal-login" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="row no-gutters">
                <div class="insertion col-lg-6">
                    <button type="button" class="close close-log" data-dismiss="modal" aria-label="Close">
                        <img class="img-fluid" src="<?= Yii::getAlias('@web/img/close.png') ?>">
                    </button>
                    <div class="container text-center">
                        <h2>LOG IN</h2>
                        <span class="info"><p>Bentornato! Accedi al tuo account.</p></span>
                        <?php
                        $signinModel = new LoginForm();
                        $form = ActiveForm::begin([
                            'action' => Url::to(['site/login']),
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false
                        ]);
                        ?>
                        <div class="form-group fg">
                            <?= $form->field($signinModel, 'email')->textInput([
                                'class' => 'form-control',
                                'placeholder' => $signinModel->getAttributeLabel('email')
                            ])->label(false); ?>
                        </div>
                        <div class="form-group fg">
                            <?= $form->field($signinModel, 'password')->passwordInput([
                                'class' => 'form-control',
                                'placeholder' => $signinModel->getAttributeLabel('password')
                            ])->label(false); ?>
                        </div>
                        <div class="justify-content-around">
                            <div>
                                <!-- Remember me -->
                                <div class="custom-control custom-checkbox">
                                    <?= $form->field($signinModel, 'rememberMe')->checkbox() ?>
                                </div>
                            </div>
                            <div>
                                <!-- Forgot password -->
                                <!--a href="">Forgot password?</a-->
                            </div>
                        </div>
                        <?= Html::submitButton(Yii::t('app', 'Accedi'), ['class' => 'btn btn-dark btn-important']) ?>
                        <?= Html::button(Yii::t('app', 'Registrati'), [
                            'class' => 'btn btn-light',
                            'data-dismiss' => 'modal',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-register'
                        ]) ?>
                        <footer>
                            <a href="#">Term of use. Privacy Policy</a>
                        </footer>
                        <?php $form::end(); ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img class="img-fluid" src="<?= Yii::getAlias('@web/img/loginReg.jpg') ?>">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Register Modal -->
<div id="modal-register" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <img class="img-fluid" src="<?= Yii::getAlias('@web/img/loginReg.jpg') ?>">
                </div>
                <div class="insertion col-lg-6">
                    <button type="button" class="close close-reg" data-dismiss="modal" aria-label="Close">
                        <img class="img-fluid" src="<?= Yii::getAlias('@web/img/close.png') ?>">
                    </button>
                    <div class="container text-center">
                        <h2><?= Yii::t('app', 'REGISTRATI') ?></h2>
                        <span class="info"><p>Completa i seguenti campi per creare il tuo account.</p></span>
                        <?php
                        $signupModel = new SignupForm();
                        $form = ActiveForm::begin([
                            'action' => Url::to(['site/signup']),
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false
                        ]);
                        ?>
                        <div class="row form-group fg">
                            <div class="col">
                                <?= $form->field($signupModel, 'firstname')->textInput([
                                    'id' => 'fname',
                                    'class' => 'form-control',
                                    'placeholder' => $signupModel->getAttributeLabel('firstname')
                                ])->label(false); ?>
                            </div>
                            <div class="col">
                                <?= $form->field($signupModel, 'lastname')->textInput([
                                    'id' => 'lname',
                                    'class' => 'form-control',
                                    'placeholder' => $signupModel->getAttributeLabel('lastname')
                                ])->label(false); ?>
                            </div>
                        </div>
                        <div class="form-group fg">
                            <?= $form->field($signupModel, 'email')->textInput([
                                'class' => 'form-control',
                                'placeholder' => $signupModel->getAttributeLabel('email')
                            ])->label(false); ?>
                        </div>
                        <div class="form-group fg">
                            <?= $form->field($signupModel, 'password')->passwordInput([
                                'class' => 'form-control',
                                'placeholder' => $signupModel->getAttributeLabel('password')
                            ])->label(false); ?>
                        </div>
                        <div class="form-group fg">
                            <?= $form->field($signupModel, 'password_confirm')->passwordInput([
                                'class' => 'form-control',
                                'placeholder' => $signupModel->getAttributeLabel('password_confirm')
                            ])->label(false); ?>
                        </div>
                        <div class="justify-content-around">
                            <div class="fg ckc">
                                <div class="custom-control custom-checkbox">
                                    <?= $form->field($signupModel, 'isManager')->checkbox(); ?>
                                </div>
                            </div>
                        </div>
                        <?= Html::button(Yii::t('app', 'Accedi'), [
                            'class' => 'btn btn-light',
                            'data-dismiss' => 'modal',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-login'
                        ]) ?>
                        <?= Html::submitButton(Yii::t('app', 'Registrati'), ['class' => 'btn btn-dark btn-important']) ?>
                        <footer>
                            <a href="#">Term of use. Privacy Policy</a>
                        </footer>
                        <?php $form::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer text-white">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
    </div>
</footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
