<?php

use common\models\Event;
use common\models\Sector;
use frontend\models\form\TicketForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $event Event
 * @var $sectors Sector[]
 * @var $categoryEvents Event[]
 */

$this->title = Yii::t('app', 'Evento {title}', ['title' => $event->title]);
?>


<div class="row">
    <div id = "div1" style="display:block" class="col-md-5">
        <img class="img-fluid rounded shadow" alt="Responsive image" src="<?= $event->getPreviewPath() ?>">
    </div>
    <div id = "div2" style="display:none" class="col-md-5">
        <div id="map" class="map"></div>
    </div>
    <div class="col-md-7">
        <div class="container" style="margin-top: 2%;">
            <h1><b><div class="text-center"><?= $event->title ?></div></b></h1><br>
            <div class="text-justify"><?= $event->description ?></div>
            <br><br>
            <div class="row">
                <div class="col-md-6">
                    <div class="text-center">L' evento si terrà a <h2><b><?= $event->location ?></b></h2> in data <h2>
                            <b><?= Yii::$app->formatter->asDatetime($event->dateTime) ?></b></h2><br></div>
                </div>
                <div class="col-md-6">
                    <button id="button-map" class="btn btn-create-sector btn-lg btn-block"
                            onclick="replace();return false">Visualizza la mappa
                    </button>
                </div>
            </div>
            <?php
            $sell = empty($event->dateTimeSellStart) ||
                empty($event->dateTimeSellEnd) ||
                (date('U') > $event->dateTimeSellStart && date('U') < $event->dateTimeSellEnd);
            if ($sell): ?>
                <div class="alert alert-success alert-event" role="alert">
                    <div class="text-center">acquista ora i tuoi biglietti selezionando il settore qui sotto!</div>
                </div>
            <?php else: ?>
                <div class="alert alert-danger alert-event" role="alert">
                    <div class="text-center">La vendita dei biglietti per questo evento inizierà in
                        data <?= Yii::$app->formatter->asDatetime($event->dateTimeSellStart) ?> e finirà in data <?= Yii::$app->formatter->asDatetime($event->dateTimeSellEnd) ?></div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="container-events">
    <table class="table table-striped table-responsive-sm">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Settore</th>
            <th scope="col">Biglietti Rimanenti</th>
            <th scope="col">Prezzo</th>
            <th scope="col">Acquista</th>
        </tr>
        </thead>
        btn-
        <tbody>
        <?php foreach ($sectors as $sector): ?>
            <tr>
                <th scope="row"><span class="badge" style="background-color:<?= $sector->color ?>">&nbsp;</span><br>
                </th>
                <td><?= $sector->description ?></td>
                <td><?= $sector->remainingTickets ?></td>
                <td><?= Yii::$app->formatter->asCurrency($sector->price); ?></td>
                <td>
                    <?php if (!$sell): ?>
                            Non acquistabile
                    <?php else: ?>
                        <?php if (isset(Yii::$app->user->identity) && !Yii::$app->user->identity->isManager()): ?>
                            <?php
                            $model = new TicketForm();
                            $model->idSector = $sector->idSector;

                            $form = ActiveForm::begin([
                                'action' => Url::to(['event/add-to-cart']),
                                'enableAjaxValidation' => true,
                                'options' => ['class' => 'm-0']
                            ]); ?>
                            <?= $form->field($model, 'idSector', ['options' => ['class' => 'm-0']])->hiddenInput(['class' => 'm-0'])->label(false); ?>
                            <div class="row">
                                <div class="col">
                                    <?= $form->field($model, 'quantity', ['options' => ['class' => 'm-0']])->textInput(['placeholder' => 'Quantità'])->label(false) ?>
                                </div>
                                <div class="col">
                                    <input type="submit" class="btn btn-create-sector btn-sm btn-block"
                                           value="Aggiungi al Carrello">
                                </div>
                            </div>
                            <?php $form::end(); ?>
                        <?php else: ?>
                            Gli account manager non possono acquistare biglietti.
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php if (count($categoryEvents) > 0 && isset($event->category)): ?>
        <hr class="my-5">
        <h2 class="text-center">Altri eventi della categoria <?= $event->category->description ?></h2>
        <div class="card-deck">
            <?php foreach ($categoryEvents as $event): ?>
                <div class="col-md-3 mb-5">
                    <div class="card event-card">
                        <a href="<?= Url::to(['event/view', 'id' => $event->idEvent]) ?>">
                            <img src="<?= $event->getPreviewPath() ?>" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title text-center"><?= $event->title ?></h5>
                            <p class="card-text"><?= $event->description ?></p>
                        </div>
                        <div class="card-footer">
                            <p><strong><?= Yii::t('app', 'Località') ?>:</strong> <?= $event->location ?><br>
                                <strong><?= Yii::t('app', 'Data') ?>
                                    :</strong> <?= Yii::$app->formatter->asDatetime($event->dateTime); ?></p>
                            <!-- <small class="text-muted"><?= $event->location ?> | <?= Yii::$app->formatter->asDatetime($event->dateTime); ?></small> -->
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

<script>
    // Initialize and add the map
    function initMap() {
        // The location of Uluru
        var uluru = {
            lat: <?php if (!empty($event->latitude)) {
                echo $event->latitude;
            }?>, lng: <?php if (!empty($event->longitude)) {
                echo $event->longitude;
            }?>};
        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 12, center: uluru});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: uluru, map: map});
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSYB-eyKge2IJBYYXnQwW7tGhud1rBuvU&callback=initMap">
</script>
<script>
    function replace() {
        if(document.getElementById("div1").style.display ==="none") {
            document.getElementById("button-map").textContent ="Visualizza la mappa";
            document.getElementById("div1").style.display="block";
            document.getElementById("div2").style.display="none";
        } else {
            document.getElementById("button-map").textContent ="nascondi la mappa";
            document.getElementById("div1").style.display="none";
            document.getElementById("div2").style.display="block";
        }
    }
</script>
