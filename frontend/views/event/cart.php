<?php

use common\models\Event;
use common\models\Ticket;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $tickets Ticket[]
 * @var $totalCount
 */

$this->title = Yii::t('app', 'Carrello');

/**
 * @var $events Event[]
 * @var $ticketEvents Ticket[]
 */
$events = [];
$ticketEvents = [];
foreach ($tickets as $ticket) {
    if (!isset($events[$ticket->sector->idEvent])) {
        $events[$ticket->sector->idEvent] = $ticket->sector->event;
        $ticketEvents[$ticket->sector->idEvent] = [];
    }
    $ticketEvents[$ticket->sector->idEvent][] = $ticket;
}
?>
<h1 class="text-center mt-3 mb-0"><?= $this->title ?></h1>
<div class="container">
    <div class="row">
      <div class="col-lg-6 tickets-cart">
        <?php foreach ($events as $event): ?>
            <div class="card ticket-list shadow bg-light mb-4 p-2">
              <div class="row">
                <div class="col-lg-5">
                  <a href="<?= Url::to(['event/view', 'id' => $event->idEvent]) ?>">
                      <img class="img-fluid rounded" alt="Responsive image" src="<?= $event->getPreviewPath() ?>">
                  </a>
                </div>
                <div class="col-lg-7">
                  <h4 class="mb-3"><b><?= $event->title ?></b></h4>
                  <?= $event->location ?><b class="text-success"><br><?= Yii::$app->formatter->asDatetime($event->dateTime) ?></b>
                </div>
              </div>
                <?php foreach ($ticketEvents[$event->idEvent] as $ticket): ?>
                    <hr>
                    <?php $sector = $ticket->sector; ?>
                    <b>Info biglietto</b>
                    Biglietto numero: <?= $ticket->info ?><br>
                    Settore: <?= $sector->description; ?><br>
                    Prezzo: <?= Yii::$app->formatter->asCurrency($sector->price) ?><br><br>
                    <a class="btn btn-outline-danger"
                       href="<?= Url::to(['event/cart-delete', 'id' => $ticket->idTicket]) ?>"><span
                                class="fa fa-trash"></span> Rimuovi dal carrello</a>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
      </div>
      <div class="col-lg-5 ml-5 summary-cart">
        <h6 class="text-center">RIEPILOGO</h6>
        <hr>
        <?php foreach ($events as $event): ?>
          <?php echo count($ticketEvents[$event->idEvent]) ?> x <b><?= $event->title ?></b>
          <span style="float:right"><?php
          $price = 0;
          foreach ($ticketEvents[$event->idEvent] as $ticket) {
            $sector = $ticket->sector;
            $price += $sector->price;
          }
          echo Yii::$app->formatter->asCurrency($price);
           ?></span><br>
          <?= $event->location ?><b class="text-success"><br><?= Yii::$app->formatter->asDatetime($event->dateTime) ?></b><br>
          <hr>
          <?php endforeach; ?>
          <b>Importo totale:</b><span style="float:right"><?= Yii::$app->formatter->asCurrency($totalCount) ?></span> <br>

        <button class="btn btn-confirm-payment mt-5" style="float:right" onclick="
                $('#pay-modal').modal('show');
                setTimeout(function () {
                location.href='<?= Url::to(['event/purchase-cart']); ?>';
                }, 2000);
                ">Conferma e paga
        </button>
      </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="pay-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="height:200px;">
                <div class="modal-body text-center align-middle">
                    <h3>Pagamento in corso <br><br><i class="fas fa-spinner fa-spin"></i></h3>
                </div>
            </div>
        </div>
    </div>
</div>
