<?php

use common\models\Category;
use common\models\Event;
use frontend\models\EventSearch;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $events Event[]
 * @var $query String
 * @var $searchModel EventSearch
 */

$this->title = Yii::t('app', 'Ricerca Evento');
?>


<?php if (count($events) == 0): ?>
    Nessun risultato.
<?php else: ?>
    <div class="row">
        <div class="col-md-2 filter-column">
            Visualizzati <?= count($events) ?> elementi.<br>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['event/search'])
            ]); ?>
            <?= $form->field($searchModel, 'title') ?>
            <?= $form->field($searchModel, 'location') ?>
            <?= $form->field($searchModel, 'idCategory')->widget(Select2::class, [
                'data' => array_merge([0 => 'Tutte le categorie'], ArrayHelper::map(Category::find()->all(), 'idCategory', 'description'))
            ]) ?>
            <input type="submit" class="btn btn-create-event btn-block">
            <?php $form::end() ?>
        </div>
        <div class="col-md-10 cards-filter">
            <div class="card-deck">
                <?php foreach ($events as $event): ?>
                    <div class="col-md-3 mb-5">
                        <div class="card event-card">
                            <a href="<?= Url::to(['event/view', 'id' => $event->idEvent]) ?>">
                                <img src="<?= $event->getPreviewPath() ?>" class="card-img-top" alt="...">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title text-center"><?= $event->title ?></h5>
                                <p class="card-text"><?= $event->description ?></p>
                            </div>
                            <div class="card-footer">
                                <p><strong><?= Yii::t('app', 'Località') ?>:</strong> <?= $event->location ?><br>
                                    <strong><?= Yii::t('app', 'Data') ?>
                                        :</strong> <?= Yii::$app->formatter->asDatetime($event->dateTime); ?></p>
                                <!-- <small class="text-muted"><?= $event->location ?> | <?= Yii::$app->formatter->asDatetime($event->dateTime); ?></small> -->
                            </div>
                        </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
