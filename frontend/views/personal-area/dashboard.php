<?php

use common\models\Ticket;
use dosamigos\chartjs\ChartJs;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use yii2fullcalendar\yii2fullcalendar;

/**
 * @var $this View
 * @var $tickets Ticket[]
 * @var $countSoldTickets int
 * @var $totalSoldTickets int
 * @var $countUnsoldTickets int
 */

$this->title = Yii::t('app', 'Dashboard manager');

$calendarEvents = [];
foreach ($events as $event) {
    $calendarEvent = new \yii2fullcalendar\models\Event();
    $calendarEvent->id = $event->idEvent;
    $calendarEvent->title = $event->title;
    $calendarEvent->url = Url::to(['personal-area/event-detail', 'id' => $event->idEvent]);
    $calendarEvent->start = date('Y-m-d', $event->dateTime);
    $calendarEvent->end = date('Y-m-d', $event->dateTime + 1);
    $calendarEvent->allDay = true;
    if (isset($event->category)) {
        $calendarEvent->color = $event->category->color;
    }
    $calendarEvents[$event->idEvent] = $calendarEvent;
}
?>
<h1 class="text-center" style="margin-top: 2%;"><?= $this->title ?></h1>

        <div class="row dashboard-row">
            <div class="col-md-4">
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th colspan="2" scope="col"><h3>Informazioni Generali</h3></th>
                    </tr>
                    </thead>
                <tr>
                    <td>Bigletti totali venduti</td>
                    <td><?= Yii::$app->formatter->asInteger($countSoldTickets) ?></td>
                </tr>
                <tr>
                    <td>Importo totale vendite</td>
                    <td><?= Yii::$app->formatter->asCurrency($totalSoldTickets); ?></td>
                </tr>
                <tr>
                    <td>Biglietti da vendere</td>
                    <td><?= Yii::$app->formatter->asInteger($countUnsoldTickets) ?></td>
                </tr>
                </table>
            </div>
            <div class="col-md-8 charts bg-light rounded shadow">
                <?php $firstEvents = array_splice($events, 0, 4); ?>
                <?= ChartJs::widget([
                    'type' => 'bar',
                    'options' => [
                        'height' => 200,
                        'width' => 400
                    ],
                    'data' => [
                        'labels' => ArrayHelper::getColumn($firstEvents, 'title'),
                        'datasets' => [
                            [
                                'label' => 'Percentuale di vendita',
                                'data' => ArrayHelper::getColumn($firstEvents, 'percentageSoldTickets'),
                                'backgroundColor' => "rgb(31, 40, 51)",
                            ],
                        ],
                    ],

                    'clientOptions' => [
                        'scales' => [
                            'yAxes' => [
                                'stacked' => true,
                                'ticks' => [
                                    'beginAtZero' => true,
                                    'max' => 100,
                                ]
                            ],
                        ]
                    ]
                ]); ?>
                <?php
                $start = date('U', strtotime("-15 days"));
                $end = date('U');
                $dateEnd = date('U', strtotime("-1 day", $start));
                $dateStart = $start;
                $labels = [];
                $data = [];
                while ($dateEnd <= $end) {
                    $labels[] = date('d-m', $dateStart);
                    $data[] = Ticket::find()->sold()->andWhere(['between', 'dateTime', $dateStart, $dateEnd])->count();
                    $dateStart = $dateEnd;
                    $dateEnd = date('U', strtotime("+1 day", $dateEnd));
                }
                ?>
            </div>
        </div>
        <div class="row dashboard-row">
            <div class="col-md-4">
                <div style="height: 70vh">
                    <?= yii2fullcalendar::widget([
                        'id' => 'calendar',
                        'defaultView' => 'listMonth',
                        'events' => array_values($calendarEvents),
                        'themeSystem' => 'bootstrap4',
                        'clientOptions' => [
                            'header' => [
                                'left' => 'today prev,next',
                                'center' => '',
                                'right' => 'title',
                            ],
                            'height' => 'parent',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-8 charts bg-light rounded shadow">
                <?= ChartJs::widget([
                    'type' => 'line',
                    'options' => [
                        'height' => 200,
                        'width' => 400
                    ],
                    'data' => [
                        'labels' => $labels,
                        'datasets' => [
                            [
                                'label' => 'Andamento vendite giornaliere',
                                'data' => $data,
                                'backgroundColor' => "rgb(31, 40, 51)",
                            ],
                        ],
                    ],

                    'clientOptions' => [
                        'scales' => [
                            'yAxes' => [
                                'stacked' => true,
                                'ticks' => [
                                    'beginAtZero' => true,
                                    'max' => 100,
                                ]
                            ],
                        ]
                    ]
                ]); ?>
            </div>
        </div>
