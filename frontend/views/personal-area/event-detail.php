<?php

use common\models\Event;
use common\models\Sector;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $model Event
 * @var $sectors Sector[];
 */
$formModel = new Sector();
$formModel->idEvent = $model->idEvent;

$this->title = Yii::t('app', '{title}', ['title' => $model->title]);
?>

<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <h1 class="text-center mt-3 mb-3"><?= $this->title ?></h1>
            <div class="text-justify"><strong>Descrizione: </strong><?= $model->description; ?><br><br><br></div>
            <strong>Luogo: </strong><?= $model->location ?><br>
            <strong>Data e ora: </strong><?= Yii::$app->formatter->asDatetime($model->dateTime); ?><br>
            <hr>
            <a class="btn btn-danger btn-delete-sector"
               href="<?= Url::to(['personal-area/event-delete', 'id' => $model->idEvent]) ?>"
               data-confirm="Sei sicuro di voler eliminare l'evento ?"
            ><span class="fa fa-trash"></span> Elimina Evento</a>
        </div>
        <div class="col-lg-6">
            <br>
            <h2>Nuovo settore</h2>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['personal-area/sector-create']),
                'enableAjaxValidation' => true,
            ]); ?>
            <?= $this->render('sector-form', [
                'model' => $formModel,
                'form' => $form,
          ]); ?>
        <?php $form::end(); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-10">
        <h2 class="mt-3">Settori:</h2>
        <hr>
        <?php if (count($sectors) == 0): ?>
            Nessun settore trovato
        <?php endif; ?>
        <?php foreach ($sectors as $sector): ?>
            <h3><?= $sector->description ?></h3>
            <strong>Prezzo: </strong><?= Yii::$app->formatter->asCurrency($sector->price); ?><br>
            <strong>Posti disponibili: </strong><?= $sector->maxTickets ?><br>
            <strong>Biglietti venduti: </strong><?= $sector->getTickets()->sold()->count() ?><br>
            <strong>Colore: </strong><span class="badge" style="background-color:<?= $sector->color ?>">&nbsp;</span><br>
            <a href="<?= Url::to(['personal-area/sector-delete', 'id' => $sector->idSector]); ?>"
               class="btn btn-danger btn-delete-sector mt-3"
               data-confirm="Conferma eliminazione settore"><span class="fa fa-trash"></span> Elimina settore</a>
            <hr>
        <?php endforeach; ?>
    </div>
  </div>
</div>
