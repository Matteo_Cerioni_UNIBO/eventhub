<?php

use common\models\User;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $content string
 * @var $model User
 */

$action = Yii::$app->requestedAction->id;
?>
<?php if ($model->isManager()): ?>
    <div class="row flex-fill fill d-flex">
        <div class="col-lg-2 side-bar">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <span>Benvenuto <?= Yii::$app->user->identity->name ?></span>
                <div class="container side-bar-container">
                    <a class="nav-link <?= $action == 'dashboard' ? 'active' : '' ?>"
                       href="<?= Url::to(['personal-area/dashboard']) ?>"><i class="fas fa-chart-line"></i>
                        Dashboard</a>
                    <a class="nav-link <?= $action == 'events' ? 'active' : '' ?>"
                       href="<?= Url::to(['personal-area/events']) ?>"><i class="fas fa-calendar-alt"></i> Gestione
                        eventi</a>
                    <a class="nav-link <?= $action == 'event-create' ? 'active' : '' ?>"
                       href="<?= Url::to(['personal-area/event-create']) ?>"><i class="fas fa-plus"></i> Crea evento</a>
                    <a class="nav-link <?= $action == 'detail' ? 'active' : '' ?>"
                       href="<?= Url::to(['personal-area/detail']) ?>"><i class="fas fa-cog"></i> Opzioni</a>
                    <a class="nav-link" href="<?= Url::to(['site/logout']) ?>"><i class="fas fa-sign-out-alt"></i>
                        Logout</a>
                </div>
            </div>
        </div>
        <div class="col-lg-10">
            <?= $content ?>
        </div>
    </div>
<?php else: ?>
    <?= $content ?>
<?php endif; ?>
