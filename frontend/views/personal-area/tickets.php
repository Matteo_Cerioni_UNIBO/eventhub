<?php

use common\models\Ticket;
use yii\helpers\Url;
use yii\web\View;
use yii2fullcalendar\yii2fullcalendar;

/**
 * @var $this View
 * @var $tickets Ticket[]
 */

$this->title = Yii::t('app', 'I miei biglietti');
if (count($tickets) > 0) {
    $date = date('Y-m-d', end($tickets)->sector->event->dateTime);
} else {
    $date = date('Y-m-d');
}
$calendarEvents = [];
foreach ($tickets as $ticket) {
    $sector = $ticket->sector;
    $event = $sector->event;
    if (!isset($calendarEvents[$event->idEvent])) {
        $calendarEvent = new \yii2fullcalendar\models\Event();
        $calendarEvent->id = $event->idEvent;
        $calendarEvent->title = $event->title;
        $calendarEvent->url = Url::to(['event/view', 'id' => $event->idEvent]);
        $calendarEvent->start = date('Y-m-d', $event->dateTime);
        $calendarEvent->end = date('Y-m-d', $event->dateTime + 1);
        $calendarEvent->allDay = true;
        if (isset($event->category)) {
            $calendarEvent->color = $event->category->color;
        }
        $calendarEvents[$event->idEvent] = $calendarEvent;
    }
}


/**
 * @var $events Event[]
 * @var $ticketEvents Ticket[]
 */
$events = [];
$ticketEvents = [];
foreach ($tickets as $ticket) {
    if (!isset($events[$ticket->sector->idEvent])) {
        $events[$ticket->sector->idEvent] = $ticket->sector->event;
        $ticketEvents[$ticket->sector->idEvent] = [];
    }
    $ticketEvents[$ticket->sector->idEvent][] = $ticket;
}
?>
<h1 class="text-center mt-3 mb-5"><?= $this->title ?></h1>

<div class="row">
  <div class="col-lg-4">
    <?php foreach ($events as $event): ?>
        <div class="shadow bg-light ml-5 mr-5 mb-4 p-2">
          <h4 class="mb-3"><b><?= $event->title ?></b></h4>
          <?= $event->location ?><b class="text-success"><br><?= Yii::$app->formatter->asDatetime($event->dateTime) ?></b>
            <?php foreach ($ticketEvents[$event->idEvent] as $ticket): ?>
              <hr>
                <?php $sector = $ticket->sector; ?>
                <b>Info biglietto</b>
                Biglietto numero: <?= $ticket->info ?><br>
                Settore: <?= $sector->description; ?><br>
                Prezzo: <?= Yii::$app->formatter->asCurrency($sector->price) ?><br><br>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
  </div>
  <div class="col-lg-7">
    <div class="calendar">
        <?= yii2fullcalendar::widget([
            'id' => 'calendar',
            'defaultView' => 'month',
            'events' => array_values($calendarEvents),
            'themeSystem' => 'bootstrap4',
            'clientOptions' => [
                'defaultDate' => $date,
                'header' => [
                    'left' => 'today prev,next',
                    'center' => 'title',
                    'right' => 'month,listYear',
                ],
                'height' => 'parent',
            ],
        ]); ?>
    </div>
  </div>
</div>
