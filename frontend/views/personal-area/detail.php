<?php

use common\models\User;
use yii\web\View;

/**
 * @var $this View
 * @var $model User
 */

$this->title = Yii::t('app', 'Dettaglio account');
?>
<div class="container">
    <h1 class="text-center mt-3"><?= $this->title ?></h1><br>
    Email: <b><?= $model->email ?></b><br>
    Username: <?= $model->username ?><br>
    Nome: <?= $model->name ?><br>
    Cognome: <?= $model->surname ?><br>
</div>