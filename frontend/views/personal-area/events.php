<?php

use common\models\Event;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $events Event[]
 */

$this->title = Yii::t('app', 'Gestione Eventi');
?>

<h1 class="text-center mt-3"><?= $this->title ?></h1>


<?= Yii::t('app', 'Visualizzati {count} eventi', ['count' => count($events)]) ?>


<?php if (count($events) == 0): ?>
    Nessun risultato.
<?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-deck">
                <?php foreach ($events as $event): ?>
                <div class="col-md-3 mb-5">
                    <div class="card event-card">
                    <a href="<?= Url::to(['personal-area/event-detail', 'id' => $event->idEvent]) ?>">
                        <img src="<?= $event->getPreviewPath() ?>" class="card-img-top" alt="...">
                    </a>
                    <div class="card-body">
                        <h5 class="card-title text-center"><?= $event->title ?></h5>
                        <p class="card-text"><?= $event->description ?></p>
                    </div>
                    <div class="card-footer">
                        <p><strong><?= Yii::t('app', 'Località') ?>:</strong> <?= $event->location ?><br>
                            <strong><?= Yii::t('app', 'Data') ?>
                                :</strong> <?= Yii::$app->formatter->asDatetime($event->dateTime); ?></p>
                        <!-- <small class="text-muted"><?= $event->location ?> | <?= Yii::$app->formatter->asDatetime($event->dateTime); ?></small> -->
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    </div>
<?php endif; ?>
