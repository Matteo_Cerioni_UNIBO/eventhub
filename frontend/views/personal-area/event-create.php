<?php

use common\models\Category;
use frontend\models\form\EventForm;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $model EventForm
 */

$this->title = Yii::t('app', 'Nuovo evento');
$categories = Category::find()->all();
$categoriesData = ArrayHelper::map($categories, 'idCategory', 'description');
?>

    <h1 class="new-event text-center mt-3"><?= $this->title ?></h1><br>

<?php
$form = yii\widgets\ActiveForm::begin([
    'action' => Url::to(['personal-area/event-create']),
    'options' => ['enctype' => 'multipart/form-data']
]);
?>
    <div class="container"> <!-- QUI PUOI SCRIVERE HTML COME VUOI -->
        <div class="col-lg-5 left-input">
            <?= $form->field($model, 'title') ?>
            <?= $form->field($model, 'description')->textarea() ?>
            <?= $form->field($model, 'idCategory')->widget(Select2::class, [
                'data' => $categoriesData
            ]) ?>
            <?= $form->field($model, 'dateTime')->widget(DateControl::class, [
                'type' => DateControl::FORMAT_DATETIME
            ]) ?>
            <?= $form->field($model, 'dateTimeSellStart')->widget(DateControl::class, [
                'type' => DateControl::FORMAT_DATETIME
            ]) ?>
            <?= $form->field($model, 'dateTimeSellEnd')->widget(DateControl::class, [
                'type' => DateControl::FORMAT_DATETIME
            ]) ?>
        </div>
        <div class="col-lg-5 right-input">
            <?= $form->field($model, 'location') ?>
            <?= $form->field($model, 'latitude') ?>
            <?= $form->field($model, 'longitude') ?>
            <?= $form->field($model, 'previewUpload')->widget(FileInput::class, [
                'pluginOptions' => [
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
            ]); ?>
      </div>
      <div class="col-lg-5">
        <input type="submit" value="Invia" class="btn btn-create-event mt-3">
      </div>
    </div>
<?php
$form::end();
