<?php

use common\models\Sector;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $model Sector
 * @var $form ActiveForm
 */

?>

<?= $form->field($model, 'idEvent')->hiddenInput()->label(false); ?>
<?= $form->field($model, 'idSector')->hiddenInput()->label(false); ?>
<div class="sector-data">
      <?= $form->field($model, 'description'); ?>
      <?= $form->field($model, 'price'); ?>
      <?= $form->field($model, 'maxTickets'); ?>
      <?= $form->field($model, 'color'); ?>
</div>


<input type="submit" class="btn btn-create-sector btn-block">
