<?php

use common\models\Event;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $events Event[]
 */

$this->title = 'EventHub - Homepage';
?>

<div class="site-index">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">EVENTHUB <span class="fa fa-calendar"></span></h1>
            <p>Nata nel 2020, EventHub è una società in Italia di biglietteria, marketing e commercio elettronico per
                eventi di musica, spettacolo, sport e cultura. EventHub è leader in Italia nel proprio settore con
                diversi milioni di biglietti commercializzati attraverso la propria piattaforma. Tra i suoi clienti si
                contano nella musica tutti i più importanti promoter , nello sport circa 20 squadre di calcio tra serie
                A, B, e C e gli autodromi dove ogni anno si svolgono le gare italiane di Formula 1, MotoGP e Superbike.
                E poi ancora i più noti teatri di tradizione o di intrattenimento, musical, cabaret, family show,
                festival estivi teatrali ed operistici, oltre che mostre e circuiti museali.</p>
            <a href="#section2"><span></span>Scorri</a>
        </div>
    </div>
    <section id="section2">
        <div class="container-fluid">
            <h1 class="text-center">In evidenza</h1>
            <div class="card-deck">
                <?php if (sizeof($events) >= 10): ?>
                    <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="row">
                                        <?php for($i = 0; $i<5; $i++): ?>
                                            <div class="card event-card">
                                                <a href="<?= Url::to(['event/view', 'id' => $events[$i]->idEvent]) ?>">
                                                    <img src="<?= $events[$i]->getPreviewPath() ?>" class="card-img-top" alt="...">
                                                </a>
                                                <div class="card-body">
                                                            <h5 class="card-title text-center"><?= $events[$i]->title ?></h5>
                                                    <p class="card-text"><?= $events[$i]->description ?></p>
                                                </div>
                                                <div class="card-footer">
                                                    <p><strong><?= Yii::t('app', 'Località') ?>:</strong> <?= $events[$i]->location ?><br>
                                                        <strong><?= Yii::t('app', 'Data') ?>
                                                            :</strong> <?= Yii::$app->formatter->asDatetime($events[$i]->dateTime); ?></p>
                                                    <!-- <small class="text-muted"><?= $events[$i]->location ?> | <?= Yii::$app->formatter->asDatetime($events[$i]->dateTime); ?></small> -->
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="row">
                                        <?php for($i = 5; $i<10; $i++): ?>
                                            <div class="card event-card">
                                                <a href="<?= Url::to(['event/view', 'id' => $events[$i]->idEvent]) ?>">
                                                    <img src="<?= $events[$i]->getPreviewPath() ?>" class="card-img-top" alt="...">
                                                </a>
                                                <div class="card-body">
                                                    <h5 class="card-title text-center"><?= $events[$i]->title ?></h5>
                                                    <p class="card-text truncate"><?= $events[$i]->description ?></p>
                                                </div>
                                                <div class="card-footer">
                                                    <p><strong><?= Yii::t('app', 'Località') ?>:</strong> <?= $events[$i]->location ?><br>
                                                        <strong><?= Yii::t('app', 'Data') ?>
                                                            :</strong> <?= Yii::$app->formatter->asDatetime($events[$i]->dateTime); ?></p>
                                                    <!-- <small class="text-muted"><?= $events[$i]->location ?> | <?= Yii::$app->formatter->asDatetime($events[$i]->dateTime); ?></small> -->
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"><h1><i class="fas fa-angle-left"></i></h1></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"><h1><i class="fas fa-angle-right"></i></h1></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

</div>