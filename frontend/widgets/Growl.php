<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class Growl extends Widget
{

    const SUCCESS = 'growl_success';
    const WARNING = 'growl_warning';
    const DANGER = 'growl_danger';
    const INFO = 'growl_info';
    const GROWL = 'growl_growl';
    const MINIMALIST = 'growl_minimalist';
    const PASTEL = 'growl_pastel';
    public $delay = 100;
    public $offset = 1000;
    public $align = 'left';
    public $defaultGrowlType = \kartik\growl\Growl::TYPE_GROWL;
    /**
     * @var array the Growl types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - key: the name of the session flash variable
     * - value: the bootstrap alert type (i.e. danger, success, info, warning)
     */
    protected $growlTypes = [
        self::DANGER => \kartik\growl\Growl::TYPE_DANGER,
        self::SUCCESS => \kartik\growl\Growl::TYPE_SUCCESS,
        self::INFO => \kartik\growl\Growl::TYPE_INFO,
        self::WARNING => \kartik\growl\Growl::TYPE_WARNING,
        self::GROWL => \kartik\growl\Growl::TYPE_GROWL,
        self::MINIMALIST => \kartik\growl\Growl::TYPE_MINIMALIST,
        self::PASTEL => \kartik\growl\Growl::TYPE_PASTEL,
    ];

    /**
     * add a flash message in session that render a growl on the next rendering
     * @param $type
     * @param null $message
     * @param $title
     * @param null $icon
     */
    public static function add($type, $message, $title = null, $icon = null)
    {
        Yii::$app->session->addFlash($type, ['title' => $title, 'message' => $message, 'icon' => $icon], true);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $session = Yii::$app->session;
        $types = $session->getAllFlashes();
        $delay = $this->delay;

        foreach ($types as $key => $flashes) {
            $type = ArrayHelper::getValue($this->growlTypes, $key, $this->defaultGrowlType);
            foreach ($flashes as $flash) {
                if (isset($flash['message']))
                    echo \kartik\growl\Growl::widget([
                        'type' => $type,
                        'title' => ArrayHelper::getValue($flash, 'title'),
                        'icon' => ArrayHelper::getValue($flash, 'icon'),
                        'body' => ArrayHelper::getValue($flash, 'message'),
                        'delay' => $delay,
                        'showSeparator' => true,
                        'pluginOptions' => [
                            'showProgressbar' => true,
                            'placement' => [
                                'from' => 'top',
                                'align' => $this->align,
                            ]
                        ]
                    ]);
                $delay += $this->offset;
            }
            $session->removeFlash($key);
        }
    }
}
