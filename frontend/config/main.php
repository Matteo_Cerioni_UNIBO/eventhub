<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'name' => 'EventHub',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'dd/MM/yyyy',
            'datetimeFormat' => 'dd/MM/yyyy hh:ss',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => '€',
            'nullDisplay' => '',
            'datetimeFormat' => 'php:d/m/Y H:i',
        ],

    ],
    'modules' => [

        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
            // format settings for displaying each date attribute
            'displaySettings' => [
                'date' => 'php:d/m/Y',
                'time' => 'H:i',
                'datetime' => 'php:d/m/Y H:i',
            ],

            // format settings for saving each date attribute
            'saveSettings' => [
                'date' => 'php:U',
                'time' => 'php:U',
                'datetime' => 'php:U',
            ],

            // automatically use kartik\widgets for each of the above formats
            'autoWidget' => true,
            'ajaxConversion' => false,
        ],
    ],
    'params' => $params,
];
