<?php

namespace frontend\controllers;

use common\models\Event;
use common\models\Sector;
use common\models\Ticket;
use frontend\models\EventSearch;
use frontend\models\form\TicketForm;
use frontend\widgets\Growl;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Event controller
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSearch($q = null)
    {
        if (!isset($q)) {
            $q = Yii::$app->request->post('q');
        }
        if (isset($q)) {
            $query = Event::find()->andWhere(['or',
                ['like', 'description', $q],
                ['like', 'title', $q],
                ['like', 'location', $q],
            ])->limit(200);
            $searchModel = new EventSearch();
            $searchModel->title = $q;
        } else {
            $searchModel = new EventSearch();
            if ($searchModel->load(Yii::$app->request->post())) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($searchModel);
                } else {
                    $query = $searchModel->generateQuery(Yii::$app->request->post());
                }
            } else {
                throw new BadRequestHttpException();
            }
        }
        $events = $query->all();
        return $this->render('search-results', [
            'query' => $q,
            'events' => $events,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id)
    {
        Url::remember();
        $model = $this->findModel($id);
        $sectors = $model->sectors;

        $categoryEvents = Event::find()->category($model->idCategory)->andWhere(['!=', 'Event.idEvent', $model->idEvent])->limit(4)->all();
        return $this->render('view', ['event' => $model, 'sectors' => $sectors, 'categoryEvents' => $categoryEvents]);
    }

    public function actionAddToCart()
    {
        $model = new TicketForm();
        if ($model->load($_POST)) {
            $sector = $this->findSectorModel($model->idSector);
            $event = $sector->event;
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->create()) {
                Growl::add(Growl::SUCCESS,
                    Yii::t('app', 'Sono stati aggiunti al carrello <b>{quantity}</b> biglietti per il settore <b>{sector}</b> dell\'evento <b>{event}</b>', [
                        'quantity' => $model->quantity,
                        'sector' => $sector->description,
                        'event' => $event->title,
                    ]),
                    Yii::t('app', 'Biglietto/i aggiunti al carrello.'),
                    'fa fa-cart-plus'
                );
                return $this->goBack();
            }
        }
        throw new BadRequestHttpException();
    }

    public function actionCart()
    {
        Url::remember();
        $user = Yii::$app->user->identity;
        $query = $user->getTickets()->joinWith(['sector', 'sector.event'])->carrello()->orderBy('Event.idEvent, Sector.idSector');
        $tickets = $query->all();
        if (count($tickets) == 0) {
            return $this->redirect(['site/index']);
        }
        $total = $query->sum('Sector.price');
        return $this->render('cart', ['tickets' => $tickets, 'totalCount' => $total]);
    }

    public function actionCartDelete($id)
    {
        $ticket = $this->findTicketModel($id);
        $sector = $ticket->sector;
        $event = $sector->event;
        if ($ticket->idUser != Yii::$app->user->getId()) throw new ForbiddenHttpException();
        if ($ticket->delete()) {
            Growl::add(Growl::WARNING, Yii::t('app', 'Biglietto <b>{ticket}</b> per <b>{event}</b> - <b>{sector}</b> rimosso dal carrello', [
                'ticket' => $ticket->info,
                'event' => $event->title,
                'sector' => $sector->description,
            ]));
        }
        return $this->goBack();
    }

    public function actionPurchaseCart()
    {
        $user = Yii::$app->user->identity;
        $query = $user->getTickets()->joinWith(['sector', 'sector.event'])->carrello();
        $tickets = $query->all();
        foreach ($tickets as $ticket) {
            $ticket->purchase();
        }
        Growl::add(Growl::SUCCESS,
            Yii::t('app', 'Troverà i suoi biglietti nella sua area personale.'),
            Yii::T('app', 'Grazie per l\'acquisto!'),
            'fa fa-check'
        );
        return $this->redirect(['personal-area/tickets']);
    }

    /**
     * @param $id
     * @return Event|null
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = Event::findOne($id);
        if (!isset($model))
            throw new NotFoundHttpException();
        return $model;
    }

    /**
     * @param $id
     * @return Sector|null
     * @throws NotFoundHttpException
     */
    public function findSectorModel($id)
    {
        $model = Sector::findOne($id);
        if (!isset($model))
            throw new NotFoundHttpException();
        return $model;
    }

    /**
     * @param $id
     * @return Ticket|null
     * @throws NotFoundHttpException
     */
    public function findTicketModel($id)
    {
        $model = Ticket::findOne($id);
        if (!isset($model))
            throw new NotFoundHttpException();
        return $model;
    }
}
