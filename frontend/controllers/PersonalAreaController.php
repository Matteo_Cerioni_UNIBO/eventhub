<?php

namespace frontend\controllers;

use common\models\Event;
use common\models\Sector;
use common\models\Ticket;
use frontend\models\form\EventForm;
use frontend\widgets\Growl;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Personal Area Controller
 */
class PersonalAreaController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionDetail()
    {
        Url::remember();
        $model = Yii::$app->user->identity;
        return $this->renderView($model, 'detail', ['model' => $model]);
    }

    public function renderView($model, $view, $params = [])
    {
        return $this->render('view', ['model' => $model,
            'content' => $this->renderPartial($view, $params)
        ]);
    }

    public function actionEventCreate()
    {
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();

        $model = new EventForm();
        if ($model->load($_POST)) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->create()) {
                Growl::add(Growl::SUCCESS, Yii::t('app', 'Evento creato correttamente'));
                return $this->redirect(['personal-area/event-detail', 'id' => $model->idEvent]);
            }
        }
        return $this->renderView($user, 'event-create', ['model' => $model]);
    }

    public function actionDashboard()
    {
        Url::remember();
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();

        $query = Event::find()->orderBy('Event.dateTime');
        if (!$user->isAdmin()) {
            $query->createdBy($user->id);
        }
        $events = $query->all();
        $idEvents = array_values(ArrayHelper::map($events, 'idEvent', 'idEvent'));
        $countSoldTickets = Ticket::find()->joinWith('sector')
            ->sold()->andWhere(['Sector.idEvent' => $idEvents])->count();
        $totalSoldTickets = Ticket::find()->joinWith('sector')
            ->sold()->andWhere(['Sector.idEvent' => $idEvents])->sum('Sector.price');
        $countUnsoldTickets = Sector::find()->andWhere(['Sector.idEvent' => $idEvents])->sum('maxTickets') - $countSoldTickets;
        return $this->renderView($user, 'dashboard', [
            'events' => $events,
            'countSoldTickets' => $countSoldTickets,
            'totalSoldTickets' => $totalSoldTickets,
            'countUnsoldTickets' => $countUnsoldTickets,
        ]);
    }

    public function actionEvents()
    {
        Url::remember();
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();

        $query = Event::find()->orderBy('Event.dateTime');
        if (!$user->isAdmin()) {
            $query->createdBy($user->id);
        }
        $events = $query->all();

        return $this->renderView($user, 'events', ['events' => $events]);
    }

    public function actionEventDetail($id)
    {
        Url::remember();
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();
        $model = $this->findEventModel($id);
        if (!$user->isAdmin() && $model->idUser != $user->getId()) throw new ForbiddenHttpException();

        $sectors = $model->sectors;
        return $this->renderView($user, 'event-detail', [
            'model' => $model,
            'sectors' => $sectors,
        ]);
    }

    public function actionEventDelete($id)
    {
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();
        $model = $this->findEventModel($id);
        if (!$user->isAdmin() && $model->idUser != $user->getId()) throw new ForbiddenHttpException();

        if ($model->delete()) {
            Growl::add(Growl::SUCCESS, Yii::t('app', 'Evento eliminato correttamente'));
        }
        return $this->redirect(['personal-area/events']);
    }

    public function actionEventUpdate($id)
    {
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();
        $model = $this->findEventModel($id);
        if (!$user->isAdmin() && $model->idUser != $user->getId()) throw new ForbiddenHttpException();

        if ($model->load($_POST)) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->save()) {
                Growl::add(Growl::SUCCESS, Yii::t('app', 'Evento modificato correttamente'));
                return $this->goBack();
            }
        }
        return $this->renderView($user, 'event-update', ['model' => $model]);
    }

    public function actionSectorCreate()
    {
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();

        $model = new Sector();
        if ($model->load($_POST)) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($model->save()) {
                Growl::add(Growl::SUCCESS, Yii::t('app', 'Settore creato correttamente'));
                return $this->goBack();
            }
        }
        return $this->renderView($user, 'event-create', ['model' => $model]);
    }

    public function actionSectorDelete($id)
    {
        $sector = $this->findSectorModel($id);
        $event = $sector->event;
        $user = Yii::$app->user->identity;
        if (!$user->isManager()) throw new ForbiddenHttpException();
        if (!$user->isAdmin() && $event->idUser != $user->getId()) throw new ForbiddenHttpException();
        if ($sector->delete()) {
            Growl::add(Growl::SUCCESS, Yii::t('app', 'Settore eliminato correttamente'));
        }
        return $this->goBack();
    }

    public function actionTickets()
    {
        $user = Yii::$app->user->identity;
        $tickets = $user->getTickets()
            ->sold()
            ->joinWith(['sector', 'sector.event'])
            ->orderBy('Event.dateTime desc')
            ->all();
        return $this->render('tickets', ['tickets' => $tickets]);

    }

    /**
     * @param $id
     * @return Event|null
     * @throws NotFoundHttpException
     */
    public function findEventModel($id)
    {
        $model = Event::findOne($id);
        if (!isset($model))
            throw new NotFoundHttpException();
        return $model;
    }

    /**
     * @param $id
     * @return Sector|null
     * @throws NotFoundHttpException
     */
    public function findSectorModel($id)
    {
        $model = Sector::findOne($id);
        if (!isset($model))
            throw new NotFoundHttpException();
        return $model;
    }
}
