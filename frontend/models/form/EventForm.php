<?php

namespace frontend\models\form;

use common\models\Event;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class EventForm extends Model
{
    public $title;
    public $description;
    public $dateTime;
    public $dateTimeSellStart;
    public $dateTimeSellEnd;
    public $location;
    public $latitude;
    public $longitude;
    public $previewUpload;
    public $idCategory;
    public $idEvent;

    public function rules()
    {
        return [
            [['title', 'dateTime'], 'required'],
            [['dateTime', 'dateTimeSellStart', 'dateTimeSellEnd'], 'integer'],
            [['idCategory'], 'integer'],
            [['title', 'location', 'state', 'urlPreview'], 'string', 'max' => 50],
            [['description'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['previewUpload'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEvent' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Titolo'),
            'description' => Yii::t('app', 'Descrizione'),
            'dateTime' => Yii::t('app', 'Data e ora'),
            'dateTimeSellStart' => Yii::t('app', 'Inizio vendita'),
            'dateTimeSellEnd' => Yii::t('app', 'Fine vendita'),
            'location' => Yii::t('app', 'Luogo'),
            'Latitude' => Yii::t('app', 'Latitudine'),
            'longitude' => Yii::t('app', 'Longitudine'),
            'previewUpload' => Yii::t('app', 'Immagine di Anteprima'),
            'idCategory' => Yii::t('app', 'Categoria'),
        ];
    }

    public function create()
    {
        $this->previewUpload = UploadedFile::getInstance($this, 'previewUpload');
        $event = new Event();
        $event->title = $this->title;
        $event->description = $this->description;
        $event->dateTime = $this->dateTime;
        $event->dateTimeSellStart = $this->dateTimeSellStart;
        $event->dateTimeSellEnd = $this->dateTimeSellEnd;
        $event->idCategory = $this->idCategory;
        $event->latitude = $this->latitude;
        $event->longitude = $this->longitude;
        $event->location = $this->location;

        //$event->idUser=Yii::$app->user->id;
        if ($event->save()) {
            $path = 'preview-' . $event->idEvent . '.' . $this->previewUpload->extension;
            $this->previewUpload->saveAs(Yii::getAlias('@webroot/uploads/') . $path);
            $event->urlPreview = $path;
            $result = $event->save();
            if ($result) {
                $this->idEvent = $event->idEvent;
            }
            return $result;
        }
        return false;
    }

}