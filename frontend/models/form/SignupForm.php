<?php
namespace frontend\models\form;

use common\models\User;
use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $password_confirm;
    public $isManager;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['firstname', 'trim'],
            ['firstname', 'required'],
            ['firstname', 'string', 'min' => 2, 'max' => 50],

            ['lastname', 'trim'],
            ['lastname', 'required'],
            ['lastname', 'string', 'min' => 2, 'max' => 50],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 50],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'Questo indirizzo email è già registrato.')],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_confirm', 'required'],
            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => Yii::t('app', 'Le password non corrispondono.')],

            //['terms', 'required', 'requiredValue' => true, 'message' => Yii::t('app', 'Per procedere è obbligatorio accettare i termini e condizioni.')],
            //['terms', 'boolean'],
            ['isManager', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'firstname' => Yii::t('app', 'Nome'),
            'lastname' => Yii::t('app', 'Cognome'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::T('app', 'Password'),
            'password_confirm' => Yii::t('app', 'Conferma Password'),
            //'terms' => Yii::t('app', 'Accetta termini e condizioni'),
            'isManager' => Yii::t('app', 'Organizzatore')
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     * @throws Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->name = $this->firstname;
        $user->surname = $this->lastname;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($this->isManager) {
            $user->rule = User::RULE_MANAGER;
        } else {
            $user->rule = User::RULE_CUSTOMER;
        }

        return $user->save()/* && $this->sendEmail($user)*/ ;

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
