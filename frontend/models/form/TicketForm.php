<?php

namespace frontend\models\form;

use common\models\Sector;
use common\models\Ticket;
use Yii;
use yii\base\Model;

class TicketForm extends Model
{
    public $idSector;
    public $quantity;

    public function rules()
    {
        return [
            [['quantity', 'idSector'], 'required'],
            [['quantity', 'idSector'], 'integer', 'min' => 1]
        ];
    }

    public function attributeLabels()
    {
        return [
            'quantity' => Yii::t('app', 'Quantità'),
        ];
    }

    public function create()
    {
        $sector = Sector::findOne($this->idSector);
        $event = $sector->event;
        $i = 0;
        for ($i = 0; $i < $this->quantity; $i++) {
            $ticket = new Ticket();
            $ticket->state = Ticket::STATE_CARRELLO;
            $ticket->idSector = $this->idSector;
            $ticket->dateTime = date('U');
            $ticket->idUser = Yii::$app->user->id;
            $ticket->save();
            $ticket->info = $event->idEvent . '-' . $sector->idSector . '-' . $ticket->idTicket;
            $ticket->save(false);
        }
        return true;
    }
}