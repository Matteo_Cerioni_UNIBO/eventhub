<?php

namespace frontend\models;

use common\models\Event;
use yii\base\Model;


class EventSearch extends Event
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCategory'], 'integer'],
            [['title', 'location', 'q'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function generateQuery($params)
    {
        $this->load($params);

        $query = Event::find();

        if (!empty($this->title)) {
            $query->andWhere(['like', 'title', $this->title]);
        }

        if (!empty($this->location)) {
            $query->andWhere(['like', 'location', $this->location]);
        }

        if (!empty($this->idCategory)) {
            $query->andWhere(['idCategory' => $this->idCategory]);
        }

        /*if (!empty($this->birthdayMin)) {
            $start_date = date('Ymd', strtotime(str_replace('/', '-', $this->birthdayMin)));
            $query->andFilterWhere(['>=', 'Patient.birthday', $start_date]);
        }
        if (!empty($this->birthdayMax)) {
            $end_date = date('Ymd', strtotime(str_replace('/', '-', $this->birthdayMax)));
            $query->andFilterWhere(['<=', 'Patient.birthday', $end_date]);
        }*/

        return $query;
    }
}
