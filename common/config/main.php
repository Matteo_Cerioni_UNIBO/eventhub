<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@event/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'app.php',
                    ],
                ],
            ],
        ],
    ],
    'sourceLanguage' => 'it-IT',
    'language' => 'it', // Not it-IT FOR LAUNCH I18N, if Equal not translated
    'timeZone' => 'Europe/Rome',
];
