<?php

namespace common\models;

use common\models\query\TicketQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ticket".
 *
 * @property int $idTicket
 * @property int $idSector
 * @property string|null $info
 * @property string $dateTime
 * @property string $state
 * @property int|null $idUser
 *
 * @property Sector $sector
 * @property User $user
 */
class Ticket extends ActiveRecord
{
    const STATE_CARRELLO = 'carrello';
    const STATE_ACQUISTATO = 'acquistato';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Ticket';
    }

    /**
     * {@inheritdoc}
     * @return TicketQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TicketQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idSector', 'dateTime', 'state'], 'required'],
            [['idSector', 'idUser'], 'integer'],
            [['dateTime'], 'safe'],
            [['info'], 'string', 'max' => 100],
            [['state'], 'string', 'max' => 50],
            [['idSector'], 'exist', 'skipOnError' => true, 'targetClass' => Sector::class, 'targetAttribute' => ['idSector' => 'idSector']],
            [['idUser'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['idUser' => 'idUser']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTicket' => Yii::t('app', 'id Ticket'),
            'idSector' => Yii::t('app', 'id Sector'),
            'info' => Yii::t('app', 'info'),
            'dateTime' => Yii::t('app', 'Date Time'),
            'state' => Yii::t('app', 'state'),
            'idUser' => Yii::t('app', 'id User'),
        ];
    }

    public function purchase()
    {
        $this->state = Ticket::STATE_ACQUISTATO;
        $this->dateTime = date('U');
        return $this->save();
    }

    /**
     * @return ActiveQuery
     */
    public function getSector()
    {
        return $this->hasOne(Sector::class, ['idSector' => 'idSector']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['idUser' => 'idUser']);
    }
}
