<?php

namespace common\models;

use common\models\query\MediaQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "media".
 *
 * @property int $idMedia
 * @property string $URL
 * @property int|null $idEvent
 *
 * @property Event $event
 */
class Media extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Media';
    }

    /**
     * {@inheritdoc}
     * @return MediaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MediaQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['URL'], 'required'],
            [['idEvent'], 'integer'],
            [['URL'], 'string', 'max' => 100],
            [['idEvent'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['idEvent' => 'idEvent']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idMedia' => Yii::t('app', 'id Media'),
            'URL' => Yii::t('app', 'Url'),
            'idEvent' => Yii::t('app', 'id Event'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getidEvent()
    {
        return $this->hasOne(Event::class, ['idEvent' => 'idEvent']);
    }
}
