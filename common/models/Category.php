<?php

namespace common\models;

use common\models\query\CategoryQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "category".
 *
 * @property int $idCategory
 * @property string $description
 * @property string $color
 *
 * @property Event[] $events
 */
class Category extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Category';
    }

    /**
     * {@inheritdoc}
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'color'], 'required'],
            [['description'], 'string', 'max' => 100],
            [['color'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCategory' => Yii::t('app', 'id Category'),
            'description' => Yii::t('app', 'description'),
            'color' => Yii::t('app', 'color'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::class, ['idCategory' => 'idCategory']);
    }
}
