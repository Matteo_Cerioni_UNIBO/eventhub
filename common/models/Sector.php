<?php

namespace common\models;

use common\models\query\SectorQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sector".
 *
 * @property int $idSector
 * @property int $idEvent
 * @property string|null $description
 * @property float $price
 * @property string|null $color
 * @property int $maxTickets
 *
 * @property Event $event
 * @property int $remainingTickets
 * @property mixed $ticketsCount
 * @property Ticket[] $tickets
 */
class Sector extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Sector';
    }

    /**
     * {@inheritdoc}
     * @return SectorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SectorQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEvent', 'description', 'price', 'maxTickets'], 'required'],
            [['idEvent', 'maxTickets'], 'integer', 'min' => 0],
            [['price'], 'number'],
            [['description'], 'string', 'max' => 100],
            [['color'], 'string', 'max' => 50],
            [['idEvent'], 'exist', 'skipOnError' => true, 'targetClass' => Event::class, 'targetAttribute' => ['idEvent' => 'idEvent']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idSector' => Yii::t('app', 'ID'),
            'idEvent' => Yii::t('app', 'Evento'),
            'description' => Yii::t('app', 'Descrizione'),
            'price' => Yii::t('app', 'Prezzo'),
            'color' => Yii::t('app', 'Colore'),
            'maxTickets' => Yii::t('app', 'Numero posti disponibili'),
        ];
    }

    public function getTicketsCount()
    {
        return $this->getTickets()->sold()->count();
    }

    public function getRemainingTickets()
    {
        return $this->maxTickets - $this->ticketsCount;
    }

    /**
     * @return ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::class, ['idEvent' => 'idEvent']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::class, ['idSector' => 'idSector']);
    }
}
