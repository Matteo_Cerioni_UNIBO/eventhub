<?php

namespace common\models\query;

use common\models\Event;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Event]].
 *
 * @see \common\models\Event
 */
class EventQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Event[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Event|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function createdBy($idUser)
    {
        return $this->andWhere(['Event.idUser' => $idUser]);
    }

    public function category($idCategory)
    {
        return $this->andWhere(['Event.idCategory' => $idCategory]);
    }
}
