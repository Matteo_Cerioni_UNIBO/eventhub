<?php

namespace common\models\query;

use common\models\Sector;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Sector]].
 *
 * @see \common\models\Sector
 */
class SectorQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Sector[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Sector|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
