<?php

namespace common\models\query;

use common\models\Media;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Media]].
 *
 * @see \common\models\Media
 */
class MediaQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Media[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Media|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
