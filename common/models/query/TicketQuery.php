<?php

namespace common\models\query;

use common\models\Ticket;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Ticket]].
 *
 * @see \common\models\Ticket
 */
class TicketQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ticket[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ticket|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function sold()
    {
        return $this->andWhere(['Ticket.state' => Ticket::STATE_ACQUISTATO]);
    }

    public function carrello()
    {
        return $this->andWhere(['Ticket.state' => Ticket::STATE_CARRELLO]);
    }
}
