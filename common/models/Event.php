<?php

namespace common\models;

use common\models\query\EventQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "Event".
 *
 * @property int $idEvent
 * @property string $title
 * @property string|null $description
 * @property string $dateTime
 * @property string $dateTimeSellStart
 * @property string $dateTimeSellEnd
 * @property string $location
 * @property float $latitude
 * @property float $longitude
 * @property string $state
 * @property string|null $urlPreview
 * @property int|null $idUser
 * @property int|null $idCategory
 *
 * @property User $user
 * @property Category $category
 * @property Media[] $media
 * @property mixed $percentageSoldTickets
 * @property string $previewPath
 * @property int $soldTickets
 * @property int $countTickets
 * @property Sector[] $sectors
 */
class Event extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Event';
    }

    /**
     * {@inheritdoc}
     * @return EventQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'dateTime'], 'required'],
            [['dateTime', 'dateTimeSellStart', 'dateTimeSellEnd'], 'safe'],
            [['latitude', 'longitude'], 'number'],
            [['idUser', 'idCategory'], 'integer'],
            [['title', 'location', 'state', 'urlPreview'], 'string', 'max' => 50],
            [['description'], 'string'],
            [['idUser'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['idUser' => 'idUser']],
            [['idCategory'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['idCategory' => 'idCategory']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEvent' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Titolo'),
            'description' => Yii::t('app', 'Descrizione'),
            'dateTime' => Yii::t('app', 'Data e ora'),
            'dateTimeSellStart' => Yii::t('app', 'Inizio vendita'),
            'dateTimeSellEnd' => Yii::t('app', 'Fine vendita'),
            'location' => Yii::t('app', 'Luogo'),
            'Latitude' => Yii::t('app', 'Latitudine'),
            'longitude' => Yii::t('app', 'Longitudine'),
            'state' => Yii::t('app', 'Stato'),
            'URLPreview' => Yii::t('app', 'URL Anteprima'),
            'idUser' => Yii::t('app', 'Utente'),
            'idCategory' => Yii::t('app', 'Categoria'),
        ];
    }

    public function insert($runValidation = true, $attributes = null)
    {
        $this->idUser = Yii::$app->user->id;
        return parent::insert($runValidation, $attributes);
    }

    public function getPreviewPath()
    {
        if (empty($this->urlPreview)) {
            return Yii::getAlias('@web/img/defaultPreview.jpg');
        }
        return Yii::getAlias('@web/uploads/') . $this->urlPreview;
    }

    public function getCountTickets()
    {
        $count = 0;
        $sectors = $this->sectors;
        foreach ($sectors as $sector) {
            $count += $sector->maxTickets;
        }
        return $count;
    }

    public function getSoldTickets()
    {
        $count = 0;
        $sectors = $this->sectors;
        foreach ($sectors as $sector) {
            $count += $sector->maxTickets - $sector->remainingTickets;
        }
        return $count;
    }

    public function getPercentageSoldTickets()
    {
        $soldTickets = $this->soldTickets;
        $countTickets = $this->countTickets;
        if ($countTickets > 0) {
            return ((double)$this->soldTickets) * 100 / ((double)$this->countTickets);
        }
        return 0;
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['idUser' => 'idUser']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['idCategory' => 'idCategory']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasMany(Media::class, ['idEvent' => 'idEvent']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSectors()
    {
        return $this->hasMany(Sector::class, ['idEvent' => 'idEvent']);
    }
}
