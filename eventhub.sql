-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: eventhub
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.19.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Category`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `idCategory` int(10) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `color` varchar(50) NOT NULL,
  PRIMARY KEY (`idCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

--
-- Table structure for table `User`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
                        `idUser` int(10) NOT NULL AUTO_INCREMENT,
                        `rule` varchar(50) NOT NULL,
                        `name` varchar(50) NOT NULL,
                        `surname` varchar(50) NOT NULL,
                        `email` varchar(50) NOT NULL,
                        `username` varchar(100) NOT NULL,
                        `password` varchar(100) NOT NULL,
                        `auth_key` varchar(32) NOT NULL,
                        PRIMARY KEY (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

--
-- Table structure for table `Event`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Event` (
  `idEvent` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT '',
  `dateTime` datetime NOT NULL,
  `dateTimeSellStart` datetime NOT NULL,
  `dateTimeSellEnd` datetime NOT NULL,
  `location` varchar(50) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `state` varchar(50) NOT NULL,
  `urlPreview` varchar(50) DEFAULT '',
  `idUser` int(10) DEFAULT NULL,
  `idCategory` int(10) DEFAULT NULL,
  PRIMARY KEY (`idEvent`),
  KEY `IdUser` (`idUser`),
  KEY `IdCategory` (`idCategory`),
  CONSTRAINT `Event_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Event_ibfk_2` FOREIGN KEY (`idCategory`) REFERENCES `Category` (`idCategory`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Dumping data for table `Event`
--


--
-- Table structure for table `Media`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Media` (
  `idMedia` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `idEvent` int(10) DEFAULT NULL,
  PRIMARY KEY (`idMedia`),
  KEY `IdEvent` (`idEvent`),
  CONSTRAINT `Media_ibfk_1` FOREIGN KEY (`idEvent`) REFERENCES `Event` (`idEvent`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Media`
--


--
-- Table structure for table `Sector`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sector` (
  `idSector` int(10) NOT NULL AUTO_INCREMENT,
  `idEvent` int(10) NOT NULL,
  `description` varchar(100) DEFAULT '',
  `price` float NOT NULL,
  `color` varchar(50) DEFAULT '',
  `maxTickets` int(10) NOT NULL,
  PRIMARY KEY (`idSector`,`idEvent`),
  KEY `IdEvent` (`idEvent`),
  CONSTRAINT `Sector_ibfk_1` FOREIGN KEY (`idEvent`) REFERENCES `Event` (`idEvent`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sector`
--


--
-- Table structure for table `Ticket`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ticket` (
  `idTicket` int(10) NOT NULL AUTO_INCREMENT,
  `idSector` int(10) NOT NULL,
  `info` varchar(100) DEFAULT '',
  `dateTime` datetime NOT NULL,
  `state` varchar(50) NOT NULL,
  `idUser` int(10) DEFAULT NULL,
  PRIMARY KEY (`idTicket`,`idSector`),
  KEY `IdSector` (`idSector`),
  KEY `IdUser` (`idUser`),
  CONSTRAINT `Ticket_ibfk_1` FOREIGN KEY (`idSector`) REFERENCES `Sector` (`idSector`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Ticket_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `User` (`idUser`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ticket`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-05 18:16:40


alter table Event modify description TEXT null;
alter table Event modify dateTime int not null;
alter table Event modify dateTimeSellStart int null;
alter table Event modify dateTimeSellEnd int null;

